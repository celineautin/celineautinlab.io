---
title: "A Saint-Sulpice-le-Dunois, des habitants travaillent sur le patois marchois"
subtitle: Reportage / France Bleu Creuse
date: 2019-02-25
tags: ["culture", "patois", "Creuse", "CNRS"]
---

**Des habitants de Saint-Sulpice-le-Dunois se sont réunis pour travailler sur le "parler marchois", dans le cadre d'une étude linguistique du CNRS.**

![A chaque mot français son équivalent marchois ... ou presque !](/marchois.jpg)

_"Le gratte-cul de l'églantier ... tiens, comment on dit ça déjà ?"_ Devant sa grille de mots, Odette est perplexe. Elle cherche l'équivalent en patois marchois du fruit de l'églantier. _"On passe"_, décide-t-elle après un moment de flottement. Dans la salle des fêtes de Saint-Sulpice-le-Dunois (Creuse), huit habitants se sont réunis à l'initiative d'un bénévole, Pascal Fournioux, pour répondre à une grille établie par le CNRS.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/marchois.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Cette grille se présente selon un système d'équivalence : à chaque mot français, les bénévoles doivent retrouver l'équivalent patois et le retranscrire phonétiquement. Animaux, végétaux, parties du corps et aliments : le questionnaire du CNRS est très complet sur les noms ; les bénévoles pourront aussi s'attaquer aux pronoms et à la conjugaison du parler marchois s'ils le souhaitent.

## Un travail de retranscription difficile

_"Le plus compliqué c'est de passer de l'oral à l'écrit"_, confie Pascal Fournioux. _"Le patois n'est justement pas destiné à être écrit !"_ D'autant plus qu'il existe de multiples variantes du parler marchois, selon le lieu où l'on habite : _"d'un village à l'autre, ça peut changer, bien sûr !"_ sourit Odette. _"Moi je ne suis pas de Saint-Sulpice-le-Dunois même, et j'utilise des mots un peu différents"._

>Poisson, c'est pesson ou pessou, selon d'où l'on est.  Roger, participant au groupe de travailhttps://www.untoitpourlesabeilles.fr/inscription.html?eid=1

## Une étude du CNRS sur le long cours

Le patois marchois concerne toute une frange nord du Massif central, en forme de croissant :

![Le croissant marchois est une" zone tampon" entre langue d'occ et d'oil. Capture d'écran et copyright : https://atlas.limsi.fr/](/patois.jpg)

Le groupe de travail de Saint-Sulpice-le-Dunois n'est pas le seul chez nous à remplir ces questionnaires : à Crozant ou à Fursac sont aussi menées des enquêtes dans le cadre du projet "Les parlers du Croissant".

<iframe src="https://www.google.com/maps/d/embed?mid=15sF_lH--rndKshFW9Ws8Uk4bVQJfxfNC" width="640" height="480"></iframe>
