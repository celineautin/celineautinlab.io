---
title: "Les maternelles de Felletin ont fait leur Grande lessive (et ça n'est pas ce que vous croyez)"
subtitle: Reportage / France Bleu Creuse
date: 2019-03-28
tags: ["art", "culture", "Creuse", "maternelle"]
---

**_Les enfants de l'école maternelle de Felletin viennent de participer à "La Grande lessive", une initiative pour faire entrer l'art contemporain dans les lieux publics. Les enfants ont réalisé des dessins, des peintures, et des collages qu'ils ont ensuite accroché sur une corde à linge dans la cour._**

![Des dessins, une corde à linge : l'installation la "Grande lessive" a fière allure dans la cour de la maternelle de Felletin.](/lessive.png)

"La Grande lessive", ce sont les enfants qui l'expliquent le mieux : "On fait des dessins, de la peinture, des collages. Et puis les maîtresses vont les accrocher pour les faire sécher." A Felletin, les 42 élèves de la maternelle viennent de passer la journée à réaliser cette installation éphémère : une corde à linge avec leurs œuvres inspirées des techniques d'art contemporain.

<figure>
    <figcaption>Écouter le reportage :</figcaption>
<audio
        controls
        src="/lessive.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Il n'y a que deux conditions à remplir : **respecter le thème de "La Grande lessive", "De la couleur !", et utiliser des feuilles A4.** Les enfants sont libres d'improviser à partir de ce thème, et ça plaît beaucoup à Samira : "J'ai fait des spirales, des points, des escargots et des cœurs. D'habitude on fait pas ça, c'est la première fois, et j'ai envie de faire ça tout le temps !"

![Un atelier pour travailler la nuance de couleurs](/lessive_2.jpg)

Sans le savoir, **les enfants utilisent toutes les techniques de l'art contemporain** : le collage à la Picasso, la projection de peinture comme Andy Warhol, l'art corporel, l'abstraction ... et une fois terminées, direction la cour de l'école pour accrocher les œuvres sur la corde à linge !

"_On ne va pas ramener les dessins chez nous"_, regrette Marius.  C'est le principe de l'installation en art, mais il peut se consoler : les parents curieux sont nombreux à contempler la corde à linge, à la recherche des œuvres de leurs enfants. Parmi eux, Julie trouve l'idée excellente : _"C'est très chouette que la cour soit investie comme ça !"_

C'est la deuxième année que l'école participe à ce projet, et à voir les enfants s'amuser dans chaque atelier, Sylvie Olmos, maîtresse en maternelle, sait que le pari est réussi : _"Ils sont très réceptifs, beaucoup plus ouverts que nous à de nouvelles formes. Et ils voient beaucoup plus de choses que les adultes."_ **Les maîtresses sont même prêtes à faire une nouvelle lessive l'année prochaine.**
