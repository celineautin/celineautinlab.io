---
title: "Abeilles creusoises recherchent parrains et marraines"
subtitle: Reportage / France Bleu Creuse
date: 2019-06-12
tags: ["agriculture", "abeilles", "Creuse", "pesticides"]
---

**_En Creuse, il est possible de parrainer les abeilles d'Alban Héritier dans le cadre de l'opération "Un toit pour les abeilles". En échange, les parrains et marraines reçoivent du miel, des photos des abeilles et peuvent visiter les ruches._**

![Alban Héritier, apiculteur amateur creusois, participe à l'opération "Un toit pour les abeilles"](/abeilles.png)

Au fond d'un verger, entouré par les marguerites et les cerisiers, Alban Héritier bichonne trois ruches, en amateur, depuis maintenant trois ans. Pour faire connaître son miel, il vient de s'inscrire à l'opération "Un toit pour les abeilles", qui permet aux particuliers et aux entreprises de parrainer des abeilles.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/abeilles.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Avec ses 30 ruches, Alban Héritier est le premier Creusois à participer à cette opération. _"Je n'ai pas hésité quand j'ai vu de quoi il s'agissait_, explique-t-il. _D'ordinaire, les gens me demandent souvent comment ça se passe, certains m'accompagnent près des ruches pour voir comment on fait du miel. Le public est intéressé."_ Selon Alban Héritier, [l'annonce de la disparition des abeilles l'année dernière](https://www.francebleu.fr/infos/agriculture-peche/videos-abeilles-en-danger-les-chiffres-cles-1528297904) a aussi fait bouger les mentalités.

**Tout le monde peut parrainer**, des entreprises aux particuliers, [selon une grille de tarifs bien précis](https://www.untoitpourlesabeilles.fr/parrainage-particulier) : _"C'est précieux de se sentir soutenu,_ estime Alban Héritier. _Le but du parrainage, ça n'est pas de gagner de l'argent, car je suis apiculteur amateur, mais avec avec l'argent récolté je vais pouvoir accueillir plus d'abeilles"_. Il va également pouvoir s'équiper, en ruches (60 euros minimum), en pots de miel et en cire.

**En contrepartie, l'apiculteur s'engage à partager la vie de la ruche** : visites guidées, photos régulières, portes ouvertes ... Loin d'être une contrainte, un plaisir pour Alban Héritier, avide de partage sa passion.

## Protéger et déguster le miel creusois

Autre aspect important du parrainage : _"C'est une manière de soutenir l'apiculture creusoise_, souligne Alban Héritier. _On a la chance de vivre dans un département plutôt sain, d'avoir une campagne encore préservée. Il faut manger du miel creusois, arrêter le miel asiatique, mauvais pour la santé."_ La France produit actuellement 15 000 tonnes de miel par an, alors que les Français en consomment deux fois plus. Parrainer des ruches, c'est donc encourager la production locale et réduire les importations.

>Mangez du miel creusois ! Alban Héritier, apiculteur

Alban Héritier espère aussi que l'opération de parrainage changera les habitudes en matière de pesticides : _"Au moment de choisir entre deux produits, peut-être qu'un parrain ou une marraine réfléchira et choisira le moins nocif pour les abeilles",_ explique-t-il. Il espère faire des émules chez les autres apiculteurs creusois.

Si vous souhaitez participer à l'opération de parrainage, **il faut s'inscrire en ligne** sur le site ["Un toit pour une abeille"](https://www.untoitpourlesabeilles.fr/inscription.html?eid=1). Pour suivre les activités d'Alban Héritier, apiculteur amateur, rendez-vous sur sa [page Facebook](https://www.facebook.com/Apiculteur-amateur-creusois-2035666649987479/).
