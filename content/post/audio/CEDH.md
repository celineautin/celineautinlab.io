---
title: "La Cour européenne des droits de l'Homme interpelle la France sur la surpopulation carcérale"
subtitle: Reportage / France Culture
date: 2020-01-30
tags: ["CEDH", "droits de l'homme", "prison", "Observatoire nationale des prisons", "OIP"]
---

_"La France doit résorber définitivement la surpopulation carcérale"_ : c'est la Cour européenne des droits de l'Homme qui l'exige. La CEDH était saisie par 32 détenus français accompagnés par l'Observatoire nationale des prisons. Manque d'intimité, présence de nuisibles, absence de travail et de conseillers d'insertion : leurs conditions de détention étaient assimilables selon eux à des "traitements inhumains et dégradants". L'OIP espérait un arrêt pilote qui contraigne la France à agir : ça n'est pas le cas, mais l'Observatoire voit tout de même dans cette décision une victoire.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/CEDH.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>
