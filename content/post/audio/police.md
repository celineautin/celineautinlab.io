---
title: "Réforme des retraites : les policiers scientifiques dans la rue"
subtitle: Reportage / France Culture
date: 2020-01-16
tags: ["police", "retraites", "Paris", "conflit"]
---

**_Reportage auprès des policiers scientifiques en grève contre la réforme des retraites._**

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/police.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Ils ont jeté leurs blouses faute d'avoir été reçus par le Ministère de l'Intérieur : les experts de la police scientifique ont clamé leur opposition au projet de loi sur la réforme des retraites. Venus de toute la France, une centaine d'agents sur les 2.000 que compte la profession, ont reconstitué une scène de crime géante devant la pyramide du Louvre. Ces experts réclament le droit de partir à la retraite plus tôt en raison de la pénibilité de leur métier, comme leurs collègues de la police nationale, dont le régime spécial est maintenu.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Mobilisation de la police scientifique contre la réforme des retraites ce midi devant l&#39;opéra <a href="https://twitter.com/hashtag/Manifestation?src=hash&amp;ref_src=twsrc%5Etfw">#Manifestation</a> <a href="https://twitter.com/hashtag/reformesdesretraites?src=hash&amp;ref_src=twsrc%5Etfw">#reformesdesretraites</a> <a href="https://t.co/iguI6zAHOD">pic.twitter.com/iguI6zAHOD</a></p>&mdash; JB de la Torre (@Jb_DeLaTorre) <a href="https://twitter.com/Jb_DeLaTorre/status/1225025933482110976?ref_src=twsrc%5Etfw">February 5, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
