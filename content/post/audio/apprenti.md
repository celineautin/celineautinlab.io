---
title: "Rencontre avec Abdoulaye, un apprenti en or"
subtitle: Reportage primé / École de journalisme de Sciences Po
date: 2019-12-31
tags: ["reportage", "apprentissage", "mineurs isolés", "bâtiment", "prix"]
---
**_Rencontre avec Abdoulaye Sow, un apprenti qui a eu "le coup de foudre" pour son métier. Reportage récompensé par l'AJCAM._**

![Aboudlaye et sa classe au CFA de Trappes](/apprenti.jpg)

Même à 7 heures du matin, Abdoulaye Sow conserve le sourire. À 18 ans, il est apprenti constructeur de routes en région parisienne. Ce métier, il y est venu par hasard, après s'être réfugié en France à 15 ans. Si le travail est dur, Aboudlaye y investit des espoirs d'émancipation et d'être "bien considéré".  

<figure>
    <figcaption> Écoutez le reportage :</figcaption>
<audio
        controls
        src="/apprenti.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

**Ce reportage a remporté le grand prix de la 1ère édition du concours ["Les talents à la une"](https://ajcam.org/concours-2019-jeunes-journalistes-les-talents-a-la-une/)**, en 2019.
