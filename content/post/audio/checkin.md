---
title: "Le festival Check in Party réussit sa première soirée avec Patti Smith, Clara Luciani et Jeanne Added"
subtitle: Reportage / France Bleu Creuse
date: 2019-08-23
tags: ["radio", "spectacle", "festival", "culture", "musique", "Patti Smith", "Creuse"]
---

**_Après des mois de préparation, le festival Check-in Party réussit un sans-faute pour sa soirée de lancement. Autour notamment des chanteuses Clara Luciani, Jeanne Added et de Patti Smith, un public de tous âges et de tous horizons géographiques s'est retrouvé sur l'aérodrome de Saint-Laurent._**

![Au fil de cette première soirée, les festivaliers se sont rassemblés devant une seule scène sur l'aérodrome de Saint-Laurent](/checkin.png)

_"J'ai dansé sur les épaules de maman devant la scène et je me suis beaucoup amusée"_. Isaure, 7 ans et demie, reprend son souffle après le concert de Clara Luciani sur l'aérodrome de Saint-Laurent, près de Guéret (Creuse). Originaires du Var, ses parents ont fait le voyage sans hésiter pour cette première soirée qui avait tout pour attirer les festivaliers, et en premier lieu son affiche alléchante. De l'avis unanime des festivaliers sur place, les organisateurs ont su trouver la "bonne formule" pour assurer le spectacle.

## Un festival à la "bonne taille"

La remarque revient souvent dans la bouche des festivaliers : Check-in Party n'est peut-être pas le plus grand festival de France cet été, mais il est à taille humaine. Pour les habitués des festivals comme Adélie, originaire de Paris, c'est même une bonne surprise : "on a de l'espace, on ne se sent pas pressés par la foule quand on écoute un concert. Au lieu de faire 15 kilomètres pour aller d'une scène à l'autre, tout est plus concentré."

Simon, un Creusois peu habitué aux concerts, a lui le plaisir de voir les producteurs locaux mis en avant : _"On les trouve facilement sur le site, ça fait plaisir. Le cadre sous les étoiles est très beau, les bénévoles sont disponibles pour nous renseigner. en cas de besoin."_ Sur les routes, aucun bouchon n'a perturbé la circulation alors que les navettes gratuites affrétées entre Guéret et Saint-Laurent ont été largement plébiscitées par les festivaliers.

## Toutes les générations rassemblées devant la scène

Autre détail frappant de cette première soirée : tous les âges se croisent sur le tarmac de l'aérodrome. Les familles côtoient les jeunes branchés et les seniors qui se souviennent de leur premiers vinyles de Patti Smith, comme Nadine. _"Quand je l'ai entendu chanter Because the Night,_ confie-t-elle, _j'étais toute chamboulée, et ça m'a fait plaisir de voir que les jeunes autour de moi aussi."_

<iframe width="750" height="681" src="https://www.youtube.com/embed/c_BcivBprM0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Les parents ont aussi amené leurs enfants, comme Josiane son fils, _"pour qu'il découvre une légende vivante du rock et de la musique. Ça fait partie de ce que je veux lui transmettre."_ Mais Clara Luciani et Jeanne Added ont elles aussi leur public mélangé, qui chante en cœur les tubes comme "La Grenade" par exemple.

## Le message engagé de Patti Smith pour la forêt amazonienne

Très attendu des festivaliers, le concert de Patti Smith n'a déçu personne dans le public. Et ce qui a marqué pendant sa prestation, c'est son message de résistance et de lutte pour défendre l'environnement, qu'elle adresse avant d'entonner une reprise de "Beds Are Burning", de Midnight Oil :

>Le président brésilien Bolsonaro n'est rien qu'un idiot, et mon propre président [Donald Trump] aussi. S'il vous plaît, priez pour la forêt d'Amazonie. Où vont-ils s'arrêter ? Il faut qu'on s'unissent pour défendre notre planète et pour lutter contre le réchauffement climatique.

Un message très applaudi dans le public, alors que des incendies de grande ampleur ravagent actuellement l'Amazonie.
