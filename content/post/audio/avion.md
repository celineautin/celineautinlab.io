---
title: Un avion des services secrets échoué en Creuse depuis 59 ans
subtitle: Reportage / France Bleu Creuse
date: 2019-05-21
tags: ["avion", "services secrets", "Creuse", "insolite"]
---

**_L'épave d'un avion militaire gît sur le domaine de Villemoleix (Creuse) depuis un atterrissage raté il y a 59 ans. Son propriétaire actuel raconte cette histoire insolite, et la seconde vie de la carcasse._**

![L'épave est parfaitement conservée](/avion.jpg)

<figure>
    <figcaption>Écouter le reportage :</figcaption>
<audio
        controls
        src="/avion.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Le HD32, fabriqué en 1953, est utilisé par l'Armée de l'air pour des missions très spéciales : transporter des parachutistes agents secrets derrière le Rideau de fer : *"il a rempli beaucoup de missions de ce genre"* souligne Régis Alajouanine, l'actuel propriétaire du château de Villemoleix.

Mais tout s'arrête dans la nuit du 10 mai 1960. Les pilotes reviennent d'une mission d'entraînement à Châteauroux-Déols. *"Ils devaient plastiquer pour de faux la base militaire américaine qui était là-bas*, sourit Régis Alajouanine. *En rentrant, les pilotes devaient vraiment être contents, et donc se relâcher un peu."* Il est quatre heures du matin. Trois lampes seulement signalent la piste d’atterrissage, dans le noir. *"L'herbe était grasse car il avait plu dans la nuit,* explique Régis Alajouanine. *Le pilote a dépassé la première lampe, freiné, glissé, puis la deuxième, puis la troisième. Sans lumière, il a légèrement dévié sur sa droite, et là, il a fauché un poteau électrique que mon père voulait déjà faire retirer par EDF ! L'avion a fini sa course le nez dans une mare."*

![L'intérieur de l'épave](/carlingue.jpg)

L'armée décide de ne plus utiliser ce modèle d'avion. Avant qu'il ne soit complètement démonté, la famille de Régis Alajouanine décide de le racheter. La carlingue connaît alors une seconde vie : *"Ma mère a ajouté des petits meubles et décoré l'intérieur, se souvient-il. Et on a décidé entre jeunes d'en faire le lieu de nos surprise-party, de nos booms ! C'était des bons souvenirs, plein de gens du coin sont venus passer des soirées dans cet avion."*

Depuis, le temps et des dégradations (vols de hublots, tirs contre la carlingue) ont quelque peu usé l'épave. Mais elle n'est pas rouillée : *"C'est l'avantage de l'aluminium. Et si quelqu'un a besoin d'une pièce spécifique, on pourra la lui donner,* ajoute Régis Alajouanine. **_Enfin, il ne faut pas rêver : on ne verra jamais revoler cet appareil !"_** L'épave fait cependant le bonheur des curieux d'aéronautique et des promeneurs : *"s'ils me le demandent, je les laisse s'approcher pour prendre des photos"*, explique l'heureux propriétaire de l'épave.

*Reportage diffusé le 21 mai 2019.*
