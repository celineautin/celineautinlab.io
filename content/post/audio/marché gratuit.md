---
title: "Le marché gratuit de Châtelus-le-Marcheix attire les adeptes de la récup'"
subtitle: Reportage / France Bleu Creuse
date: 2019-08-25
tags: ["marché gratuit", "recyclage", "Creuse"]
---
**_A Châtelus-le-Marcheix vient d'être organisé un marché gratuit qui a fait le plein de Creusois soucieux de recyclage, mais aussi venus pour l'esprit derrière la pratique. Le principe des marchés gratuits fait d'ailleurs des émules partout en Creuse._**

![Les piles de vêtements ont diminué à vitesse grand V pendant ce marché gratuit à Châtelus-le-Marcheix.](/marché_gratuit.png)

Pas de troc, pas d'échange, pas d'argent : Geneviève, organisatrice du marché gratuit de Châtelus-le-Marcheix ce dimanche 25 août, est catégorique. **Un marché gratuit doit rester sans argent du début à la fin !**

Pas besoin de passer à la caisse à la fin : les participants peuvent venir avec des objets (vêtements, jouets, etc) mais aussi sans, et se servent comme ils le souhaitent. Cette organisation fait le bonheur des as de la couture et de la débrouille, comme Catherine : "Je cherche des vêtements en coton pour me faire des lingettes démaquillantes. Et puis aussi des fermetures éclair et des boutons".

>"On cherche à donner des habitudes aux gens"- Geneviève Hemard

Ce dimanche, le marché a attiré tellement de monde que les piles de vêtements ont diminué à vitesse grand V. Un succès qui n'étonne guère Geneviève Hemard. **Elle organise des marchés gratuits à Châtelus-le-Marcheix tous les trois mois depuis 2012** : _"On cherche à donner des habitudes aux gens. L'habitude de ne pas jeter ce qui est encore utilisable, ne pas acheter ce qui est trouvable ailleurs, et de penser aux autres avant de faire n'importe quoi avec ses objets."_

Ce marché gratuit de Châtelus-le-Marcheix marche tellement bien qu'il existe même un groupe Facebook dédié. Les participants font parfois 45 minutes de route pour y venir. Et il fait des émules : ce dimanche, Yann est venu pour voir comment ça fonctionne afin de **monter son propre marché gratuit à Bonnat** : _"J'aime bien l'esprit. On n'a pas tellement l'habitude de prendre quelque chose, pour rien du tout, ça nous fait nous sentir libres."_

Pour les amateurs, le prochain marché gratuit se trouve à Saint-Martin Sainte-Catherine, dimanche 1er septembre.
