---
title: "Dans le Poitou-Charentes, des bénévoles recensent les commerces ouverts sur une carte participative"
subtitle: Reportage / France Bleu Poitou
date: 2020-04-06
tags: ["carte", "confinement", "Poitou", "OpenStreetMap", "Covid-19"]
---
**_Pour éviter les déplacements inutiles, des cartographes bénévoles ont créé "Ça reste ouvert", une carte collaborative où tout le monde peut indiquer l'ouverture ou la fermeture d'un commerce, ainsi que ses horaires. Des passionnés dans le Poitou-Charentes nous expliquent._**

![Le centre-ville de Niort dans "Ça reste ouvert"Capture d'écran](/carte.png)

Le bar-tabac en bas de chez vous est-il toujours ouvert ? Votre boulangerie a-t-elle modifié ses horaires ? Il y a de grandes chances pour que vous trouviez la réponse en ligne, sur la carte [çaresteouvert.fr] (https://www.caresteouvert.fr/). Et si ça n'est pas indiqué, vous pouvez vous-même l'indiquer ! C'est ce à quoi s'emploient Mathieu, Charentais, et Jacques, habitant dans la Vienne, pendant le confinement.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/carte.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

## La cartographie à portée de clic

Depuis chez lui, à Vouneuil-sous-Biard, Jacques peut assouvir sa passion pour la cartographie : "*J'ai été à la pharmacie pour renouveler une ordonnance, et j'en ai profité pour demander ses horaires à la pharmacienne. En route j'ai fait la même chose pour deux boulangeries."* Une fois devant son ordinateur, Jacques se connecte sur internet à l'adresse de la carte. Puis il lui suffit de cliquer sur le commerce, d'indiquer s'il est ouvert ou non, ainsi que ses horaires.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Le nombre d&#39;informations concernant les lieux ouverts durant le confinement explose ! Merci à tous les contributeurs OpenStreetMap. Le résultat sur <a href="https://t.co/kiF2wcI4ew">https://t.co/kiF2wcI4ew</a> <a href="https://t.co/DpJbap2DqE">https://t.co/DpJbap2DqE</a></p>&mdash; Ça reste ouvert (@caresteouvert) <a href="https://twitter.com/caresteouvert/status/1243503779196153856?ref_src=twsrc%5Etfw">March 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


*"C'est à la portée de tous le monde*, estime Jacques. *On rend service à tous en allant simplement faire ses courses, et depuis son smartphone".* **Buralistes, boulangers, pharmacies, grandes surfaces, ehpads ... tout ce qu'il est possible de renseigner est disponible sur la carte.**

>On pense que les données géographiques doivent appartenir à chaque citoyen.

Le confinement et les restrictions de déplacement posent cependant un problème aux cartographes. Mathieu, à Châteauneuf-sur-Charente, a trouvé la solution : *"je téléphone aux élus locaux pour faire le point avec eux."* Deux autres contributeurs l'épaulent dans sa tâche pour une ville d'environ 3500 habitants, *"un ratio tout à fait raisonnable"*, juge-t-il.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Merci <a href="https://twitter.com/drobaux?ref_src=twsrc%5Etfw">@drobaux</a> pour les précisions, sont ajoutés sur Châteauneuf-sur-Charente :<br>- La résidence Félix Gaillard (EHPA) : <a href="https://t.co/r79I7We0XN">https://t.co/r79I7We0XN</a><br>- Le centre hospitalier (anciennement hôpital local) : <a href="https://t.co/JrlxXQhS0q">https://t.co/JrlxXQhS0q</a> (déjà référencé et mis à jour sur les tags)</p>&mdash; Matthieu Godet (@MatthGodet) <a href="https://twitter.com/MatthGodet/status/1241017648969891841?ref_src=twsrc%5Etfw">March 20, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Jacques, lui, est allé encore plus loin : il a sollicité un syndicat de pharmaciens pour que la profession mette d'elle-même à jours ses horaires d'ouverture. *"Au fil des jours, le nombre de pharmacies à jour augmente. On est partis de zéro la semaine dernière, à 1470 aujourd'hui."* Au 30 mars, 10 099 commerces ont été complétés par les contributeurs sur toute la France. Près de la moitié de ces lieux (55%) ont gardé les mêmes horaires, quand 25% avaient fermé.

## L'esprit collaboratif d'Open Street Map

Pour fonctionner, le site çaresteouvert.fr utilise les données du service cartographique OpenStreetMap, une carte construite collaborativement par des bénévoles. L'initiative vient d'ailleurs de bénévoles "Open Street mappeurs" de Montrouge. Un esprit que revendiquent Jacques et Mathieu, également membres de cette communauté : *"c'est un travail sur les communs. Certains renseignements doivent être ouvert à tous, et on pense que les données géographiques doivent appartenir à chaque citoyen."*

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Bon le Poitou manque de rouge. Belle mobilisation en tout cas ! <a href="https://t.co/f2pGbCWrrt">https://t.co/f2pGbCWrrt</a></p>&mdash; Matthieu Godet (@MatthGodet) <a href="https://twitter.com/MatthGodet/status/1244358526035689473?ref_src=twsrc%5Etfw">March 29, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Plusieurs associations promouvant le logiciel libre dans la région Poitou-Charentes participent par ailleurs à la mise à jour de çaresteouvert.fr : **APP3L** (Association Poitevine pour la Promotion des Logiciels Libres et de Linux), **Rochelug**, **GEBULL** (Gatine Et Bocage Utilisateurs de Logiciels Libres) et **LAPLLA** (L'Association pour la Promotion des Logiciels Libres en Angoumois).
