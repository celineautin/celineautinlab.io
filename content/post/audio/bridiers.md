---
title: À Bridiers, les couturières ont 2500 boutons à coudre avant cet été
subtitle: Reportage / France Bleu Creuse
date: 2019-04-20
tags: ["spectacle", "couture", "Creuse", "culture"]
---
_**Pour la 14e édition de la fresque de Bridiers (du 2 au 5 août prochain), les couturières bénévoles ont du pain sur la planche : 200 nouveaux costumes et plus de 2500 boutons à coudre ! Elles recherchent des volontaires pour leur prêter main-forte.**_

![Attablée devant ses costumes, pas question de baisser les bras pour Josiane](/bridiers1.jpg)

*"Là, regardez, ça fait dix boutons sur la veste toute seule, et quatre autres sur la manche. Quatorze multiplié par quinze tenues, je préfère même pas compter !"* Devant une photo de soldat anglais, période fin 18e siècle, Josiane élabore son plan d'attaque. La couturière fait partie de la trentaine de bénévoles qui élaborent les costumes du spectacle, et pour cette 14e édition, elle se confronte à un nouveau défi : coudre environ 2 500 boutons avant le 15 juin.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/bridiers.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

**Les bénévoles ont commencé à travailler sur les costumes et les boutons en octobre, bien plus tôt que les années précédentes.** Jean-Noël Pinaud, le metteur en scène, reconnaît la difficulté du travail : "_Certaines années il y avait plus de costumes à faire, mais c'était moins compliqué. Au Moyen-Âge, il n'y avait pas de boutons ! Là on a choisi le 18e siècle, et tout est plus exigeant. On a eu du mal à faire venir autant de boutons, déjà, sur le plan financier. Et ça exige d'être très minutieux._"

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/bridiers2.jpg" caption="Le stock de boutons à coudre">}}
  {{< figure thumb="-thumb" link="/bridiers3.jpg" caption="De plus près" >}}
  {{< figure thumb="-thumb" link="/bridiers4.jpg" caption="Le costume entier" >}}
{{< /gallery >}}
{{< load-photoswipe >}}
