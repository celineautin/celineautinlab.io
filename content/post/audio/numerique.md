---
title: "Les travailleurs du numérique entrent à leur tour en grève contre les retraites"
subtitle: Reportage / France Culture
date: 2019-12-23
tags: ["grève", "retraite", "tech", "numérique"]
---

À la longue liste des professions en grève contre la réforme des retraites, on doit désormais ajouter celle des travailleurs du numérique: développeurs, graphistes, consultants, chercheurs... Ils se sont rassemblés derrière la bannière ["On est la tech"](https://onestla.tech/). Du jamais-vu dans ce secteur peu politisé, pour un mécontentement qui dépasse la seule question des retraites :

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/numerique.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>
