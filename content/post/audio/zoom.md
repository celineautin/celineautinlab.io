---
title: "L'info 100% Poitou du mardi 14 avril 2020"
subtitle: Journal / France Bleu Poitou
date: 2020-04-14
tags: [journal", "Poitou", "confinement", "Covid-19", "coronavirus"]
---

**_Ce qu'il faut retenir de l'actualité dans la Vienne et les Deux-Sèvres ce mardi 14 avril 2020._**

<video controls width="750">

    <source src="/info14.mp4"
            type="video/mp4">

    Sorry, your browser doesn't support embedded videos.
</video>

## Un père emprisonné à Poitiers après la mort de son bébé de 3 mois

Le nourrisson avait de multiples blessures. Il est décédé à l’hôpital pour enfant de Tours ce dimanche, après avoir été gardé par son père, qui refuse de dire ce qui s’est passé. L’homme de 36 ans vient d’être envoyé à la prison de Vivonne. L’affaire est confiée à un juge d’instruction

## Un septuagénaire renversé à Saint-Julien-l’Ars  

La scène s’est passée à la sortie d’un bureau de tabac. L’homme a été percuté par le fourgon d’un livreur de colis et transporté au CHU de Poitiers toujours conscient.

## Le couvre-feu est prolongé à Poitiers.

La ville vient de l’étendre jusqu’au 11 mai,  la date probable de début du dé-confinement.

## La vente d’alcool interdite à La Crèche

Vous pouvez faire un croix sur vos apéro-vidéos si vous habitez la commune de la Crèche, dans les Deux-Sèvres. La mairie vient d’interdire la vente d’alcool dans les supermarchés et les commerces, jusqu’à la fin de semaine. L’arrêté fait suite à des débordements et à plusieurs manquements aux règles de confinement.

## « On est sortis de ce cauchemar grâce à l’Allemagne »

L’amertume de notre couple de Poitevins partis en voyage de noce en Nouvelle-Zélande. Revenus, enfin, dans le Poitou, après plus de trois semaines coincés dans le pays, et des appels répétés, mais sans succès, à un rapatriement par la France. Les deux amoureux sont rentrés grâce à un vol affrété par Berlin et ils ont bien du mal, encore à digérer l’inaction de la France.

## L’usine Fenwick redémarre doucement

Les chariots sortent à nouveau des lignes de production Fenwick. L’activité reprend doucement dans l’usine de Cenon sur Vienne : il y a moins d’ouvriers que d’habitude, et ils portent des masques, font vérifier leur température avant d’entrer sur le site et doivent respecter les distances de sécurité. Les chariots Fenwick produits sont livrés en priorité au secteur de la grande distribution.

## Les rats dévorent la fibre optique de la mairie de Poitiers

Quand le chat n’est pas là, les souris dansent ! Ou plutôt les rats, du côté de la mairie de Poitiers. Tout commence par des problèmes de connexion sur les différents sites de la commune ; pas moyen de s’envoyer des mails pour les employés de l’hôtel de ville ... Alors les équipes de maintenance vont voir d’un peu plus près les câbles : certains ont été complètement rongés jusqu’à la fibre, en dépit du plastique et du cuivre autour. D’après les dératiseurs, ces fameux câbles contiennent de l’amidon, de la nourriture bienvenue pour les rongeurs maintenant que les restaurants ont fermé ! Et ils préviennent : personne n’est à l’abri de rats affamés !

## Appel aux volontaires pour aider dans les Ehpad des Deux-Sèvres

Parmi les agents des collèges : chefs et aides de cuisine, femmes et hommes de ménage. Le département les invite à prêter main forte au personnels des Ehpad, moyennant un peu d’argent. Si vous êtes intéressé, il faut appeler le 05 49 06 79 79.

## Les centres de loisirs prêts à accueillir les enfants de soignants

Dernière semaine de cours avant les vacances scolaires et il y en a pour qui ça fait une différence : les enfants des soignants, des gendarmes, ou encore des pompiers. Ils sont gardés en ce moment par les enseignants dans les écoles. Les accueils de loisirs vont prendre le relais dans la communauté de commune Parthenay-Gâtine, du 20 avril jusqu’au 3 mai.

## Des festivals annulés, d’autres maintenus dans le Poitou

Le festival au village, de Brioux-sur-Boutonne, dans les Deux Sèvres, est annulé. Le rassemblement devait se tenir début juillet – impossible après les annonces d’Emmanuel Macron la 32e édition est donc reportée à l’année prochaine. Le festival FLIP de Parthenay lui non plus n'aura pas lieu.

Sont toujours maintenus, pour l’instant, le festival Au Fil du son à Civray dans la Vienne prévu du 23 au 25 juillet, comme les Heures Vagabondes : 13 concerts gratuits organisés tout l’été par le département de la Vienne.

## Redécouvrez l’histoire de la ville de Châtellerault   

On lutte contre la morosité tous ensemble, avec cet habitant de Châtellerault : Alex est un passionné d’histoire et d’archives, il sait aussi manier les vidéos, alors il a décidé d’allier les deux ! Le Châtelleraudais nous fait découvrir un monument ou un lieu de sa ville. Aujourd’hui, c’est le square Gambetta ou square Blossac : 5 minutes passionnantes avec de vieux plans, des photos anciennes et des explications pleines d’humour.

<iframe width="1211" height="681" src="https://www.youtube.com/embed/TnsvPoiFVZs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
