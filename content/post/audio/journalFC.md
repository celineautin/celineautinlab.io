---
title: "Le coronavirus fait une nouvelle victime en France, les autorités sanitaires enquêtent sur la source de la contamination "
subtitle: Journal de 22h / France Culture
date: 2020-02-26
tags: ["journal", "confinement", "Covid-19", "coronavirus"]
---

**_Un homme de 60 ans est décédé mardi soir à l'hôpital de la Pitié-Salpêtrière. C'est la deuxième victime du coronavirus en France. L'enquête commence pour savoir comment il a été contaminé, car l'enseignant n'avait pas fait de voyage dans une zone à risque._**


<figure>
    <figcaption>Écoutez le journal :</figcaption>
<audio
        controls
        src="/journalFC.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

**Nouvelle étape dans la propagation de l'épidémie en France.** Quatre malades hospitalisés, un deuxième mort à Paris : les autorités sanitaires recherchent le "patient zéro" à l'origine de la contamination, car la victime n'avait pas voyagé dans une zone à risque. L'hôpital de Creil, où il avait été hospitalisé dans un premier temps, a déclenché le plan blanc. Le point sur la situation avec Véronique Rebeyrotte.

**Les hôpitaux sont-ils suffisamment préparés et armés pour faire face à une épidémie d'ampleur ?** Le ministre de la Santé Olivier Véran assure que les personnels et les établissements sont prêts. 108 hôpitaux répartis dans chaque département sont mobilisables. Mais _"ça ne concerne qu'une dizaine de lits tout au plus"_ selon l'invité de ce journal, Christophe Prudhomme, porte-parole de l'Association des Médecins Urgentistes de France (AMUF). Il prévient : certains hôpitaux pourraient être poussés à la rupture avec cette nouvelle crise sanitaire.

**Dans le monde, une quarantaine de pays sont désormais touchés par le coronavirus.** L'Amérique du Sud connaît un premier cas au Brésil. Et l'épidémie touche jusqu'aux pays les plus fermés, comme la Corée du Nord. Les explications d'Elise Delève.

## Les autres titres du journal

**La banque BNP Paribas a sciemment trompé ses clients, selon la justice française.** Sa branche crédits Cetelem vient d'être condamnée pour pratique commerciale trompeuse. En cause : un prêt immobilier indexé sur le franc suisse, qui a endetté à vie plus de 4 600 emprunteurs. BNP Paribas doit désormais leur verser des dizaines de millions d'euros. Le reportage d'Anne-Laure Chouin au tribunal.

**Une nouvelle présidente intérimaire à l'Académie des César.** Margaret Menegoz remplace Alain Terzian, démissionnaire. Elle va devoir assurer les affaires courantes, notamment la 45e cérémonie de vendredi soir, et affronter la crise que traverse l'institution, accusée de manquer de diversité et de transparence. Son portrait par Ersin Leibowitch.
