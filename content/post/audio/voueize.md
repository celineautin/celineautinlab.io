---
title: "A Chambon-sur-Voueize, habitants et vacanciers voient peu à peu disparaître les deux rivières du bourg "
subtitle: Reportage / France Bleu Creuse
date: 2019-08-23
tags: ["sécheresse", "Creuse", "environnement"]
---
**_Avec la sécheresse, à Chambon-sur-Voueize, tout le monde s'inquiète pour les deux rivières qui traversent le bourg : la Tardes et la Voueize, cette dernière étant complètement à sec. Du jamais-vu pour ses habitants et les vacanciers qui randonnent le long des cours d'eau._**

![A Chambon-sur-Voueize, la Voueize n'est plus qu'un amas de flaques stagnante](/voueize1.png)

La Creuse serait-elle en train de perdre progressivement une partie de ce qui fait son charme, à savoir ses cours d'eau ? La question se pose à Chambon-sur-Voueize (Creuse), où les deux rivières qui traversent le bourg, la Tardes et la Voueize, sont dans une situation critique. La Voueize n'est qu'un amas apparent de flaques stagnantes. Pour les habitants et les vacanciers de passage, la situation est un désastre.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/voueize.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

C'est au-dessus du pont roman, à cheval sur la Voueize, que Patrice découvre l'étendue des dégâts. Sac à dos sur les épaules, le Savoyard suit une balade au fil de l'eau ... enfin en théorie, puisque il n'y a plus d'eau à contempler ! _"J'avais entendu à la radio parler de l'arrêté sécheresse sur le département,_ confie-t-il. _Mais je ne m’imaginais pas ça. C'est vraiment très bas."_

>Les couleurs des rivières, leurs bruits : on a l'impression d'avoir perdu quelque chose. Cécile Creuzon, maire de Chambon-sur-Voueize

Parmi les cailloux bien visibles, l'herbe pousse drue et l'eau stagne au lieu de couler. Sur les rives, Jessica et Philippe, originaires de Bretagne, n'en reviennent pas : _"Ça fait très mal au cœur de voir ça. On n'est pas revenus depuis cinq ans, et on tombe de haut."_ Le couple s'apprête à suivre le sentier de randonnée le long des gorges de la Voueize, _"sans savoir à quoi on doit s'attendre"_. De fait, les paysages de ce sentier sont transformés, car le chemin suit désormais un lit à sec.

![La Voueize est envahie par l'herbe (depuis le pont roman de Chambon-sur-Voueize)](/voueize2.jpg)

Pour la maire de Chambon-sur-Voueize, Cécile Creuzon, c'est un peu l'âme du bourg qui disparaît avec ses rivières : _"On ne s'appelle pas Chambon-sur-Voueize par hasard quand même ! L'eau faisait partie de notre quotidien. Cette perte, même si elle n'est que visuelle, donne une ambiance triste. Les couleurs des rivières, leurs bruits : on a l'impression d'avoir perdu quelque chose."_

Même crève-coeur chez les habitants historiques de Chambon-sur-Voueize, comme Jocelyne. Elle a perdu des plaisirs tout simple depuis que la Voueize est à sec : _"Autrefois on allait près de l'eau avec ma petite-fille, faire trempette, jeter une ligne pour la pêche, ou tout simplement regarder l'eau. Pour le moment plus rien n'est possible"_, témoigne-t-elle. Une disparition qui l'inquiète beaucoup pour l'avenir des cours d'eau sur sa commune et dans toute la Creuse.
