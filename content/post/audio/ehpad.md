---
title: "Skype, lettres, dessins : ils donnent le sourire aux résidents confinés de l'Ehpad de Migné-Auxances"
subtitle: Reportage / France Bleu Poitou
date: 2020-04-02
tags: ["ehpad", "confinement", "Poitou", "Covid-19", "coronavirus"]
---
**_Avec le durcissement des mesures de confinement dans les Ehpads, il faut faire preuve d'imagination pour maintenir le lien avec les résidents, obligés de rester dans leur chambre. Exemple à l'Ehpad Les Fougères de Migné-Auxances (Vienne) et avec le collège Louis-Merle, à Secondigny (Deux-Sèvres)._**

![Une vingtaine de résidents se sont mis à Skype pour discuter avec leurs familles Joy Moine](/ehpad.png)

*"C'est un travail de tout les instants"*, beaucoup intense qu'en  temps normal. Joy Moine, animatrice à l'EHPAD Les Fougères de  Migné-Auxances (Vienne) se charge de ramener un peu de gaité aux résidents confinés de l'établissement, privés des visites de leur famille et obligés de rester dans leur chambre, une mesure encouragée par le gouvernement.

<figure>
    <figcaption>Écoutez le reportage :</figcaption>
<audio
        controls
        src="/ehpad.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

## Moins d'activités pour les résidents

Cet isolement accru, Joy Moine l'a constaté chez certains résidents : *"Certaines personnes ont vraiment besoin de lien, et se disent qu'elles voudraient bien sortir, déjeuner avec les autres."* L'animatrice doit désormais elle-même venir auprès des résidents dans leur chambre, *"avec un chariot, chargé de jeux, des mandalas, des postes CD, pour se distraire."* Un nouveau temps plus individualisé dédié également à la discussion.

Des activités très réduites par rapport à la normale, et qui se déroulent, évidemment, en respectant les mesures barrières, et en portant des masques redécorés pour l'occasion : *"Certains résidents se sont un peu moqués de nous la première fois en nous voyant, ils nous ont dit que c'était le carnaval,* sourit Joy Moine. *On  s'est dit qu'on allait rendre ces masques un peu plus gais, on les a décorés au crayon, puis on a cherché des sourires humoristiques, ou bien des moustaches, sur internet."* Les soignants ont aussi adopté ces masques d'un nouveau genre.

**La fin des visites des familles reste le sujet le plus  douloureux pour les résidents**. Mais être confiné ne veut pas dire être coupé du monde. Tout le personnel de l'EHPAD est mobilisé pour maintenir le lien avec les proches des résidents : *"On a pris des photos soignants-résidents qu'on a envoyé aux familles. Bien sûr ils peuvent toujours recevoir du courrier"* détaille Joy Moine.

De plus, sur la cinquantaine de résidents que compte l'Ehpad, 20 se sont mis à Skype : *"C'est un émerveillement pour certains,* sourit l'animatrice. *Même si on est de l'autre côté de l'écran, il peut y avoir beaucoup d'émotions chez les résidents"*.

## Envoyez vos lettres aux résidents de l'EHPAD !

**L'animatrice lance désormais un appel à toutes les familles confinées** : *"On veut prouver aux résidents que la vie à l'extérieur, malgré le confinement, ne s'est pas arrêtée. Envoyez-nous  des lettres, des petits mots, des dessins, par courrier ou par mail.  Les résidents sont très demandeurs, en particulier quand ça vient des enfants."* Une manière aussi de soutenir le personnel de l'Ehpad, très éprouvé par la période.

Un appel que n'ont pas attendu les élèves du collège Louis-Merle, dans les Deux-Sèvres.  Ils viennent de transmettre une soixantaine de dessins, de petits mots et de poèmes à l'Ehpad de Secondigny ainsi qu'à l'Hôpital Nord Deux-Sèvres.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Un weekend solidaire pour les élèves de <a href="https://twitter.com/LouisMerle79?ref_src=twsrc%5Etfw">@LouisMerle79</a> : dessins, cartes mentales, lettres et poèmes envoyés au collège, et qui seront postés à l&#39;attention des personnels soignants de <a href="https://twitter.com/hashtag/hopitalnorddeuxsevres?src=hash&amp;ref_src=twsrc%5Etfw">#hopitalnorddeuxsevres</a> <br> <a href="https://twitter.com/hashtag/SolidariteHopitaux?src=hash&amp;ref_src=twsrc%5Etfw">#SolidariteHopitaux</a> <a href="https://twitter.com/DeuxSevres?ref_src=twsrc%5Etfw">@DeuxSevres</a> <a href="https://twitter.com/acpoitiers?ref_src=twsrc%5Etfw">@acpoitiers</a> <a href="https://twitter.com/DSDEN79?ref_src=twsrc%5Etfw">@DSDEN79</a> <a href="https://t.co/5W9OiaGnDV">pic.twitter.com/5W9OiaGnDV</a></p>&mdash; Collège Louis Merle (@LouisMerle79) <a href="https://twitter.com/LouisMerle79/status/1244993513038786560?ref_src=twsrc%5Etfw">March 31, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

C'est la principale, Raphaëlle Couvreux-Baudouin, qui les a incités à préparer ces courriers : *"Pendant  le weekend, les élèves me demandaient si leurs mots suffisaient, et  puis certains parents m'ont dit qu'ils avaient participé aussi. Les retours étaient enthousiastes."*

Le collège organise en effet régulièrement des activités avec les résidents de l'Ehpad de Secondigny : centenaire de la guerre 14-18, concours national de la Résistance. *"Les soutenir était donc tout naturel pour le collège"*, souligne la principale, qui réfléchit à d'autres initiatives dans ce sens.
