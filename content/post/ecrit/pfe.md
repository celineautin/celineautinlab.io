---
title: "La lutte est dans le pré : quand des éleveurs tentent de faire légaliser le soin par les plantes"
date: 2020-05-06
subtitle : Projet de fin d'études / École de journalisme de Sciences Po
tags : ["agriculture", "plantes", "environnement", "élevage"]
---

Avoir le droit de soigner les animaux d’élevage par des plantes : voilà ce que réclame depuis plusieurs années un collectif d’éleveurs, de vétérinaires et d'herboristes, _Plantes en élevage_. Pour sortir de l’illégalité, ils se tournent maintenant vers les parlementaires. Récit d’un activisme discret.

![Réunion d'information autour du collectif *Plantes en élevage*, Assemblée nationale, 5 février 2020. Crédits : Sandrine Le Feur] (/parlement.jpg)
<div class="caption">Réunion d'information autour du collectif *Plantes en élevage*, Assemblée nationale, 5 février 2020. Crédits : Sandrine Le Feur</div>

_« Il y a deux, trois ans, on déguisait ce qu’on faisait. Mais plus maintenant. »_ La voix d’Éric Guihery, éleveur bio en Mayenne, ne trahit aucune gêne. Même si c’est illégal, il soigne ses 70 vaches laitières avec des plantes, aidé par sa femme, Patricia. _« Dans notre secteur, il n’y avait aucun vétérinaire compétent sur le sujet,_ ajoute-t-il. _On s’est formés pour pouvoir utiliser des huiles essentielles pures ou en mélange. »_ Si un pis s’enflamme, l’agriculteur administre lui-même une solution à base d’huile de tournesol et d’huile essentielle pour éviter les antibiotiques. _« Maintenant, je l’inscris dans mon cahier d’exploitation »_, souligne Éric Guihery. Au risque que ce registre des soins soit consulté par les autorités, et qu'il soit sanctionné.


## Des pratiques hors-la-loi qui intéressent les parlementaires

Pourtant, si Éric Guihery parle aussi librement du sujet, c’est qu’il est largement soutenu : par 1052 collègues agriculteurs, signataires du <a href="http://www.plantesenelevage.fr/manifeste.php" target="_blank"> *Manifeste des éleveurs hors-la-loi*, </a> mais aussi par le collectif militant _Plantes en élevage_, et par plusieurs parlementaires. Parmi eux, Joël Labbé, sénateur divers gauche du Morbihan, et Sandrine Le Feur, députée LREM du Finistère, elle-même agricultrice, accompagnent le collectif depuis près d’un an.

<figure>
    <figcaption> Sandrine Le Feur, députée LREM du Morbihan, explique son soutien au collectif :</figcaption>
<audio
        controls
        src="/LeFeur.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Pour Sandrine Le Feur, rien de plus naturel. Cette mère de famille, passionnée de nature, expérimente elle-même le soin par les plantes. _« C’est une histoire de croyance, au fond,_ avoue-t-elle avant de se reprendre : _ce sont des méthodes efficaces, qui ont fait leurs preuves mais qui ont été mises de côté, peut-être en raison des intérêts pharmaceutiques. »_ La députée En Marche est aussi séduite par le caractère transpartisan du collectif : _« Il est en cours de structuration, mais ce qui est intéressant, c’est qu’il n’est pas corporatiste. Ce n’est pas un syndicat unique, une association, ou un métier : ce sont plusieurs forces, plusieurs personnes qui s’intéressent au sujet et tentent de faire des choses. »_

De son côté, Joël Labbé est un habitué des questions « écolos ». La <a href="https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000028571536&categorieLien=id" target="_blank">loi sur l’encadrement de l’utilisation des pesticides</a>, c’est lui. Au Sénat, il vient de défendre une proposition de loi _« pour un élevage éthique, soucieux du bien-être animal »._ Joël Labbé est aussi un fin connaisseur de la question des plantes médicinales, en tant que rapporteur, en 2018, d’une <a href="http://www.senat.fr/rap/r17-727/r17-7271.pdf" target="_blank">mission d’information sur le développement de l’herboristerie.</a>

Le duo s’implique auprès du collectif *Plantes en élevage* au cours de l’année 2019 : *« Ils m’ont tenue au courant de leurs avancées. On a aussi échangé des avis sur les sujets »,* explique Sandrine Le Feur, prudente. Un rôle de conseillère de l’ombre, doublé de celui de spin doctor : _« On a parlé de leur manière de communiquer. Quand ils ont évoqué l’idée du manifeste, évidemment qu’il fallait y aller. On était ensemble pour faire ça »,_ s’enthousiasme-t-elle.

## Un « coup médiatique » et politique

Ce manifeste, Éric Guihery le décrit comme un _« coup médiatique »_ qui a _« bien fonctionné »._ Le 16 octobre 2019, _Le Parisien_ offre une pleine page à ses signataires. Ils font également l'objet de reportages dans le journal de 8h de RTL, et sur les antennes locales de France Bleu et de France 3. Quelques jours plus tard, un dossier entier leur est consacré dans le journal _La Croix_. Avec un argument principal mis en avant : la lutte contre l’antibiorésistance.

<a href="https://www.facebook.com/joel.labbe.90/posts/2982818848399701" target="_blank">![Post Facebook] (/labbe.jpg)</a>

L’efficacité des antibiotiques se réduit à force d’une utilisation massive et répétée en santé humaine et animale. Depuis 2015, ce phénomène fait l’objet d’un plan d’action mondial de l’OMS. En France, <a href="https://www.anses.fr/fr/system/files/170419-Plan-ecoantibio2.pdf" target="_blank">deux plans Ecoantibio successifs</a>, lancés par le Ministère de l’agriculture, visent à réduire l'utilisation d'antibiotiques en santé animale. _« La prise de conscience citoyenne est de plus en plus forte,_ assure Éric Guihery. *Mais les <a href="https://www.anses.fr/fr/system/files/ANMV-Ra-Antibiotiques2018.pdf" target="_blank">chiffres de 2019</a> sont mauvais »*. Les partisans des plantes se présentent comme une alternative, en misant sur davantage de transparence dans les pratiques d’élevage.

<figure>
    <figcaption> Éric Guihery, "entré en résistance" pour ne plus avoir à mentir aux consommateurs :</figcaption>
<audio
        controls
        src="/guihery.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Le manifeste d’octobre s’accompagne d’<a href="https://www.mesopinions.com/petition/politique/plantes-elevage-soutenons-1052-eleveurs-eleveuses/74532?fbclid=IwAR0hIcka3FCOmDLd5ueYJcbJt4f1DFKilEZNLEji8sUtwrlUSkjzJGXIzeM" target="_blank">une pétition</a> signée par un peu plus de 44.000 internautes. _« Plus on parle du sujet, mieux c’est,_ approuve Sandrine Le Feur. _Quand on sent que les Français s’approprient un sujet, le gouvernement suit »_. En pleine séquence d’agribashing et de doutes sur les pratiques d’élevage, le mouvement peut se targuer d’attirer la sympathie. Pour ses partisans, il témoigne d’un mouvement de fond dans l’agriculture française. _« Les éleveurs sont très en demande,_ assure Lucile Brochot, vétérinaire. _Y compris en agriculture conventionnelle. »_ Elle assure des formations à la phytothérapie à la demande des chambres d’agriculture, comme celle de Saône-et-Loire. _«  Pourtant, encore trop peu de choses sont faites pour développer ces approches par les plantes : les vétérinaires sont encore méfiants, le cadre réglementaire trop contraignant. »_

<figure>
    <figcaption> Lucile Brochot, vétérinaire qui assure des formations auprès des éleveurs :</figcaption>
<audio
        controls
        src="/brochot.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

## La politique à l’assaut de la réglementation

Faire sauter ce verrou administratif : c’est le but de Sandrine Le Feur et de Joël Labbé. Selon le code de la Santé publique français, les plantes utilisées pour soigner sont considérées comme des médicaments vétérinaires. À ce titre, elles doivent obtenir une autorisation de mise sur le marché, ce que très peu possèdent. Il s’agit en fait de mesurer la toxicité des plantes qui pourraient se retrouver dans l’alimentation humaine. Ce calcul est très compliqué et très coûteux : <a href="/these.pdf" target="_blank">autour de 70.000 euros l’étude</a>. Et il a le don d’agacer Sandrine Le Feur : _« Mes vaches mangent des plantes dans les prairies,_ remarque-t-elle, _que ce soit des orties, de l’herbe ou de la luzerne avec leurs principes actifs. Après, nous consommons la viande et le lait sans être malades pour autant. Je ne comprends pas pourquoi il faudrait faire plus attention si on extrait la substance active de l’ortie pour la donner en médicament »._

<object data="/anses2013.pdf" type="application/pdf" width="750px" height="700px">
    <embed src="/anses2013.pdf">
        <p> Ce navigateur n'affiche pas les PDF. Vous pouvez le télécharger pour le voir : <a href="/anses2013.pdf">Télécharger le PDF</a>.</p>
    </embed>
</object>

La députée veut jouer les relais entre le collectif et le Ministère de l’agriculture : _« je suis là pour les assister dans leurs démarches et leur ouvrir les portes plus facilement »_. Elle soutient également un changement de législation, afin de créer une nouvelle catégorie juridique pour les plantes. Sandrine Le Feur s’appuie pour cela sur les travaux de l’Itab, l’Institut technique de l’agriculture biologique, qui fournit états des lieux et recommandations au collectif _Plantes en élevage_. *« Cette catégorie juridique comporte <a href="http://www.itab.asso.fr/downloads/el-sante/sante-liste-plantes-11mars2019.pdf" target="_blank">223 plantes</a>,* décrit Catherine Experton, membre de l’Itab, _choisies pendant des années avec des vétérinaires et des éleveurs qui les utilisent. Les plantes dangereuses sont exclues. Mais c’est un raisonnement qui est très loin de celui de l’ANSES »_. Là où le collectif *Plantes en élevage* se réclame de pratiques collectives et ancestrales, l’ANSES fait valoir <a href="/anses2018.pdf" target="_blank"> le manque d’études scientifiques poussées </a> concernant l’impact des molécules issues de plantes sur la santé humaine. _« On tourne en rond avec l’administration,_ fulmine Michel Bouy, vétérinaire et partisan des soins par les plantes. _Il faut passer un échelon au-dessus. »_

Cet échelon, c’est celui du Parlement. Depuis 2016, l’exécutif est régulièrement mis face à la question : au Sénat en 2016 et 2020 avec des questions posées par <a href="http://www.senat.fr/basile/visio.do?id=qSEQ160521729" target="_blank"> Philippe Bas (LR) </a> et <a href="https://www.senat.fr/basile/visio.do?id=qSEQ200113963" target="_blank"> Yves Détraigne (Union centriste) </a> et à l’Assemblée en 2017 et 2020, avec les questions des députées <a href="/questionBatho.pdf" target="_blank"> Delphine Batho (EELV) </a> et Sandrine Le Feur (LREM).


<object data="/questionLeFeur.pdf" type="application/pdf" width="750px" height="700px">
    <embed src="/questionLeFeur.pdf">
        <p> Ce navigateur n'affiche pas les PDF. Vous pouvez le télécharger pour le voir : <a href="/questionLeFeur.pdf">Télécharger le PDF</a>.</p>
    </embed>
</object>

Cet activisme prend un tour nouveau après la publication du manifeste. Sandrine Le Feur et Joël Labbé organisent deux réunions d’information à l’Assemblée et au Sénat, à la fin du mois de janvier 2020. _« On a essayé d’agréger le plus de parlementaires possible, de tous bords politiques. La plupart maîtrisent bien le sujet agricole ou avaient déjà entendu parler du manifeste »_, détaille Sandrine Le Feur. À l’image de Gaël Grosmaire, collaborateur parlementaire de Gérard Menuel, député LR de l’Aube, qui a assisté à la réunion. _« Gérard Menuel est un ancien céréalier, et moi j’élève des moutons,_ sourit Gaël Grosmaire. _Je ne suis pas en agriculture biologique, mais j’ai commencé à travailler sur l’utilisation de plantes ou de dérivés pour éviter les antibiotiques »_. L’auditeur a priori idéal pour le collectif, mais Gaël Grosmaire reste circonspect : _« Si on légalise cette utilisation, on s’engage dans une démarche officielle et étatique. Est-ce que demain, on ne viendra pas nous le reprocher, à l’occasion d’un scandale sanitaire ? »_.

<figure>
    <figcaption> Gaël Grosmaire, collaborateur parlementaire, a assisté à la réunion d'information :</figcaption>
<audio
        controls
        src="/grosmaire.mp3">
            Your browser does not support the
            <code>audio</code> element.
</audio>
</figure>

Des craintes qu’entend Sandrine Le Feur, mais qui ne la découragent pas. _« C’est compliqué de mobiliser sur un tel sujet, mais on a des discussions passionnantes. Regardez aussi ce qui se fait sur le cannabis thérapeutique et le chanvre. Ce serait bien de s’agréger à tout cela »_. Un vœu pieux pour l’instant, puisque son collège député Jean-Baptiste Moreau, rapporteur de la mission d’information sur le cannabis thérapeutique et lui-même agriculteur, _« ne s’est jamais intéressé »_ à la question des plantes en élevage. _« Mais nous avons deux nouveaux ministres de la Santé et de l’Agriculture, Olivier Véran et Didier Guillaume, qui semblent plus ouverts sur le sujet que leurs prédécesseurs »_ ajoute-t-elle. Elle espère lancer une mission d’information sur le sujet, suivie d’une proposition de loi.

_« Rien n’avance, et tout avance »_, soupire Michel Bouy. En l’absence de grande loi cadre sur l’agriculture à l’horizon, _« il faut continuer à faire de l’agitation, essayer de renforcer notre position vis-à-vis de l’administration pour montrer qu’on n’est pas des doux rêveurs »_ indique-t-il. Si la France se montre volontariste sur le sujet, elle pourra influencer la position de la Commission européenne, chargée de délivrer un rapport sur le soin par les plantes d’ici 2027. Mais la crise sanitaire du coronavirus a porté un coup d’arrêt brutal aux efforts du collectif : _« tout est en suspens »_, accorde Éric Guihery, dépité. En attendant, l’huile de tournesol et l’essence de géranium auront toujours droit de cité dans sa ferme.

*Céline Autin*
