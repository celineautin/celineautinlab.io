---
title: "Quand le site parodique Le Gorafi imagine une guerre civile entre la Creuse du Nord et la Creuse du Sud"
subtitle: Reportage / France Bleu Creuse
date: 2019-08-02
tags: ["parodie", "Creuse"]
---

**_La site parodique Le Gorafi s'intéresse au département de la Creuse et imagine un échange de tirs entre la Creuse du Nord et la Creuse du Sud, comme une parodie du conflit entre les deux Corée. Les commentaires amusés vont bon train sur les réseaux sociaux._**

![Regains de tensions entre la creuse du Nord et la creuse du Sud ... d'après le Gorafi](/gorafi.png)

Creusois, Creusoises, sortez vos casques et tous aux abris anti-projectiles ! Le site parodique Le Gorafi vient d'imaginer un échange de tirs entre la Creuse du Nord et la Creuse du Sud. Et détaille les opérations militaires et diplomatiques en cours :

>Des manœuvres décriées par la Creuse du Nord qui exige le départ des troupes Puydômoises stationnées sur le territoire de la Creuse du Sud. « Nous ne tolérerons plus cet impérialisme puydômois » ajoute un porte-parole du parti unique au pouvoir en Creuse du Nord.

Sont aussi décrites les tentatives de conciliation de la part du voisin cantalien :

>Une situation prise très au sérieux, même si le récent sommet organisé dans le Cantal entre le leader de la Creuse du Nord et le président du Puys de Dôme semblait avoir acté un possible arrêt du programme de patatorisation de la Creuse du Nord.

Un article où est également évoqué le "programme de patatorisation de la Creuse du Nord" et le rôle ambigu des puissances voisines :

>Mais les experts pointent du doigt cependant le rôle trouble de la Vienne mais surtout de l’Indre, partenaire stratégique de la Creuse du Nord, à la manœuvre pour maintenir son hégémonie sur cette partie du Limousin.

Avec cet article, on croit lire entre les lignes une parodie drolatique  de la situation entre la Corée du Nord et la Corée du Sud.

## Des commentaires amusés sur les réseaux sociaux

Nul besoin de préciser qu'en quelques minutes, cet article a déchaîné les commentaires sur la page Facebook du Gorafi !
