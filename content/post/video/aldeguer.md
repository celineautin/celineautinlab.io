---
title: La dessinatrice Hélène Aldeguer au plus près de la jeunesse tunisienne
subtitle: Reportage / École de journalisme de Sciences Po
date: 2018-12-01
tags: ["bande dessinée", "Tunisie", "culture", "art"]
---

Une rencontre avec Hélène Aldeguer, dessinatrice qui **fait paraître en 2018 *Après le printemps. Une jeunesse tunisienne* aux éditions Futuropolis.**

> Deux ans après la révolution, c'est ce moment où l'amertume gagne, où la jeunesse se sent désenchantée. C'est ce que j'ai voulu montrer dans mon dessin.

<video controls width="750">

    <source src="/aldeguer.mp4"
            type="video/mp4">

    Sorry, your browser doesn't support embedded videos.
</video>

Un reportage réalisé avec Minji Suh (JRI).

*Après le printemps* a reçu le Prix de la Fondation Raymond Leblanc de la jeune création 2017 et le Prix de la bande dessinée politique France Culture 2019.

Pour en savoir plus sur le travail d'Hélène Aldeguer, rendez-vous sur son [site internet](https://helenealdeguer.com/).
