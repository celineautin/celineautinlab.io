---
title: "VIDÉO - Confinement : les élèves du Conservatoire de Poitiers jouent pour les résidents des Ehpad"
subtitle: Reportage / France Bleu Poitou
date: 2020-04-20
tags: ["musique classique", "conservatoire", "Ehpad", "confinement", "Covid-19", "coronavirus"]
---

**_A l'initiative du CCAS de Poitiers, les élèves musiciens du Conservatoire de Poitiers se filment en train de jouer chez eux, puis la vidéo est envoyée aux résidents des Ehpads et des résidences autonomie. Un moment de partage inter-générationnel pour briser la solitude du confinement._**

<video controls width="750">

    <source src="/conservatoire.mp4"
            type="video/mp4">

    Sorry, your browser doesn't support embedded videos.
</video>

Dans la famille Darnajou, tout le monde a son instrument : Manon, 17 ans, à la flûte traversière, Lucie, 14 ans, à la flûte à bec et leur mère, Maïté, à la harpe. Élèves du Conservatoire de Poitiers, elles ont tout de suite dit oui quand leurs professeurs leur ont proposé de jouer pour les résidents des Ehpads et résidences autonomie du CCAS de Poitiers.

## Un moment de partage inter-générationnel

Toute la famille s'est mobilisée pour répondre à l'appel du CCAS : *"Au début*, explique Maïté, *j'ai envoyé des auditions filmées que j'avais passées auparavant. Par la suite ce sont mes filles qui ont pris le relais avec des morceaux qu'elles étaient en train de travailler"*. Quand Manon interprète un morceau, c'est un peu comme si elle jouait pour sa propre grand-mère : *"je sais qu'elle adore nous entendre jouer. Alors j'imagine que ça doit faire plaisir à ces grand-pères et grand-mères qui n'ont pas de musique en ce moment."*

>La musique prend tout son sens quand elle est partagée. Maïté Darnajou

Pour sa sœur Lucie, c'est aussi l'occasion de faire connaître des instruments méconnus, comme la flûte à bec. Sans pression, puisqu'à la moindre fausse note, *"on peut réenregistrer et se filmer à nouveau*, souligne Manon en souriant. *Ça donne un résultat impeccable."* La mise en vidéo est assurée avec beaucoup de soin par leur mère : *"sur l'une des vidéos, on voit notre chien. J'ai aussi mis des fleurs, parce que je me souviens que ma propre grand-mère aimait beaucoup voir des fleurs. Ça dépasse la musique, c'est la vie qu'on veut leur envoyer aussi."*

<iframe src="https://www.dailymotion.com/embed/video/x7tdn9l" allowfullscreen="" allow="autoplay" width="640" height="360" frameborder="0"></iframe>

**De l'autre côté de l'écran, il y a des résidents comme Monique Bougoin, à l'Ehpad René Crozet (Poitiers).** Ces vidéos sont un petit coin de ciel bleu dans sa journée, qui la ramènent plusieurs années en arrière : *"Quand j'étais petite, j'avais demandé à mes parents d'apprendre à jouer du piano. C'était pendant la guerre, ils ne pouvaient pas... alors je me réjouis toujours quand les enfants font de la musique, ça me revigore !"*

Plus d'une cinquantaine de vidéos ont déjà été produites, des plus jeunes élèves (7-8 ans) aux plus confirmés, en musique, mais aussi en danse. Certains professeurs du Conservatoire s'y sont aussi mis, comme Marc Brochet. Ce professeur de jazz a pris son accordéon pour enregistrer des chansons de l'époque des personnes âgées. Devant l’implication de ses musiciens, le directeur du Conservatoire, Éric Valdenaire, pense à l’après : *"Au moment où on va revenir, on essaiera de faire une petite tournée dans les Ehpads, pour qu'il y ait un lien direct entre les élèves et ceux qui les ont écouté à distance. Pourquoi pas l'année prochaine ?"*

<iframe frameborder="0" width="640" height="360" src="https://www.dailymotion.com/embed/video/x7tdn9m" allowfullscreen allow="autoplay"></iframe>

## Malgré le confinement le Conservatoire continue d'assurer des cours

Pour le Conservatoire de Poitiers, il a fallu s'organiser rapidement afin de poursuivre les cours à distance. Chez les Darnajou, les jeunes filles s'enregistrent via Whatsapp ou jouent de concert avec leurs professeurs, par vidéo, en direct : *"même si le son n'est pas super, c'est mieux que rien"*, grimace Lucie.

Les professeurs du Conservatoire gardent aussi le lien avec leurs élèves grâce à des recommandations : *"chaque semaine ils conseillent de regarder certains concerts, certaines pièces de théâtre, telle ou telle vidéo pour continuer à se cultiver"*, indique Éric Valdenaire. *Evidemment c'est quand il y a des soucis matériels que leur absence se fait le plus ressentir : "quand une corde se casse, quand une anche de saxophone se brise, c'est un problème pendant le confinement !"*

Le directeur du Conservatoire a conscience de ces difficultés mais souhaite aussi souligner des côtés positifs : *"les élèves apprennent à être autonomes. Quand on leur demande d'envoyer des vidéos aux Ehpads, ce sont eux qui choisissent les morceaux, qui les exécutent seuls à la maison, qui les envoient.. et ça permet aux professeurs de réécouter plusieurs fois."* Certains enseignants songent même à garder certaines méthodes de travail acquises avec le confinement, voire à changer leur pédagogie.
