---
title: EN IMAGES - Découvrez la septième tapisserie de la série Tolkien
subtitle: Reportage / France Bleu Creuse
date: 2019-05-27
tags: ["Tolkien", "tapisserie", "Creuse", "culture"]
---

**_Le carton de la future tapisserie Mithrim vient d'être présenté à la Cité internationale de la tapisserie. C'est le 7ème de la série, et il va être envoyé à un lissier pour une tombée de métier en octobre prochain._**

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/tapisserie1.jpg" caption="Le carton entier">}}
  {{< figure thumb="-thumb" link="/lissier.jpg" caption="Les instructions pour le lissier" >}}
  {{< figure thumb="-thumb" link="/montagne.jpg" caption="Un détail mystère" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

Le carton, grandeur nature, est monumental : 5 mètres 41 de long sur presque deux mètres de large. Il a fallu trois mois à Delphine Mangeret, dessinatrice-cartonnière, pour terminer le carton de Mithrim, la nouvelle tapisserie de la série Tolkien que vient de présenter la Cité de la tapisserie d'Aubusson.  

{{< tweet 1133021974626742273 >}}

**Tout commence avec une aquarelle dessinée par Tolkien lui-même.** Il faut ensuite passer au carton grandeur nature, c'est-à-dire à ce qui va servir de guide pour le tapissier. Pour réussir, Delphine Mangeret a travaillé sur des reproductions agrandies, qu'elle a ensuite peintes. _"De cette manière je peux saisir les nuances entre le foncé, le moyen et le clair."_, explique-t-elle.  

**Un carton, c'est comme un code à destination du lissier, qui réalisera la tapisserie finale** : _"Il y a des pointillés par exemple, qui créent une transition dans la couleur. Ça permet de donner des effets de fondus, qu'on remarque quand on est près de la tapisserie, mais pas de loin.",_ souligne-t-elle. _"Je fais et je refais souvent ces couleurs. Pas plus tard qu'hier même !"_  

#### Découvrez les secrets de fabrication de la tapisserie en vidéo :  

<video controls width="750">

    <source src="/tolkien.mp4"
            type="video/mp4">

    Sorry, your browser doesn't support embedded videos.
</video>

### Une mystérieuse montagne en arrière-plan

La tapisserie Mithrim aura sa petite part de mystère. Le lac est entouré de montagnes flamboyantes, surplombé d'un ciel qui domine le tableau tout entier. Au centre, on distingue une sorte de montagne qui semble cracher du feu. *"Impossible de dire ce que c'est,* sourit Delphine Mangeret. *La forme était présente sur l'aquarelle dessinée par Tolkien. Mais il n'y en a aucune trace dans les textes du Silmarillon qui parlent de Mithrim."*
