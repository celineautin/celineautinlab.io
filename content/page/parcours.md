---
title: Mon parcours
subtitle:
comments: false
---
Passionnée de radio, j'ai d'abord suivi une formation en recherche avant de me tourner vers le journalisme.

### 2018 - 2020 - Master de journalisme  
#### École de Journalisme de Sciences Po, Paris  

- Mars 2020 - Avril 2020 : apprentissage, FRANCE BLEU POITOU
- Décembre 2019 - Février 2020 : apprentissage, FRANCE CULTURE  
- Janvier 2019 - Août 2019 : apprentissage, FRANCE BLEU CREUSE  

Grand prix décerné par l'AJCAM (Association des Journalistes de la Construction) dans le cadre de la 1ère édition du concours "Les talents à la Une".

### 2014 - 2018 - Licence, Master d'anglais  
#### École Normale Supérieure de Lyon  

- Janvier 2018 - Juin 2018 : stage, *Les Pieds sur terre*, FRANCE CULTURE  
- Juillet 2017 – Août 2017 : stage, *Matinale d’été*, FRANCE INTER                                  
- Juin 2016 – Juillet 2016 : stage, Rédaction Actualités, RCF  
- Octobre 2015 – Juin 2016 : bénévolat, CAM FM
- Octobre 2014 – Janvier 2018 : bénévolat, TRENSISTOR WEBRADIO  

- 2016 : Agrégation d'anglais  
